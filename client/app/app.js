import m from 'mithril';

class CustomerService {
    static instance=null;

    // Return singleton
    static get() {
        if(!CustomerService.instance)
            CustomerService.instance=new CustomerService();
        return CustomerService.instance;
    }

    // m.request triggers redraw when its promise chain is completed
    getCustomers() {
        return m.request({method: "GET", url: "/customers"}).then((result) => {
            return result;
        });
    }

    getCustomer(customerId) {
        return m.request({method: "GET", url: "/customers/"+customerId}).then((result) => {
            return result;
        });
    }

    addCustomer(name, city) {
        return m.request({
            method: "POST",
            url: "/customers",
            data: {name, city}
        }).then((result) => {
            return result;
        });
    }
}

class Menu {
    view() {
        return  (
                <div>
                    <div>Menu: <a href="/">Customers</a></div>
                </div>
        );
    }
}

class CustomerListComponent {
    status="";
    customers=[];
    newCustomerName="";
    newCustomerCity="";
    
    constructor() {
        CustomerService.get().getCustomers().then((customers) => {
            this.customers=customers;
            this.status="successfully loaded customer list";
        }).catch((reason) => {
            this.status="error: "+reason;
        });
    }

    onNewCustomer = (e) => {
        e.preventDefault();
        const name = this.newCustomerName;
        const city = this.newCustomerCity;
        CustomerService.get().addCustomer(name, city).then((id) => {
            this.customers.push({id, name, city});
            this.status="successfully added new customer";
            this.newCustomerName="";
            this.newCustomerCity="";
        }).catch((reason) => {
            this.status="error: "+reason;
        });
    };

    view() {
        let listItems = this.customers.map((customer) => {
            return <li key={customer.id}><a href={"/#/customers/"+customer.id}>{customer.name}</a></li>
        });

        return (
            <div>
                {"status: "+this.status}
                <ul>
                    {listItems}
                </ul>
                <form onsubmit={this.onNewCustomer}>
                    <label>Name:<input type="text" name="name" value={this.newCustomerName}
                                       onchange={m.withAttr('value', (value)=>{this.newCustomerName=value})}/></label>
                    <label>City:<input type="text" name="city" value={this.newCustomerCity}
                                       onchange={m.withAttr('value', (value)=>{this.newCustomerCity=value})}/></label>
                    <input type="submit" value="New Customer"/>
                </form>
            </div>
        );
    }
}

class CustomerDetailsComponent {
    status="";
    customer={};

    constructor() {
        CustomerService.get().getCustomer(m.route.param('customerId')).then((customer) => {
            this.customer=customer;
            this.status="successfully loaded customer details";
        }).catch((reason) => {
            this.status="error: "+reason;
        });
    }

    view() {
        return (
            <div>status: {this.status}<br/>
                <ul>
                    <li>name: {this.customer.name}</li>
                    <li>city: {this.customer.city}</li>
                </ul>
            </div>
        );
    }
}

const routes = {
    "/": {
        render() {
            return <div><Menu/><CustomerListComponent/></div>
        }
    },
    "/customers/:customerId": {
        render() {
            return <div><Menu/><CustomerDetailsComponent/></div>
        }
    }
};

m.route.prefix("#");
m.route(document.getElementById("root"), "/", routes);
